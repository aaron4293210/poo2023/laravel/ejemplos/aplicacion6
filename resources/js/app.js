import './bootstrap';


import * as bootstrap from 'bootstrap';



document.addEventListener('submit', validacionFormulario);

function validacionFormulario(e) {
    const formulario = e.target;

    if (!formulario.classList.contains('needs-validation')) {
        return;
    }

    // Si el formulario no es válido, previene el evento de envío y detiene la propagación
    if (!formulario.checkValidity()) {
        e.preventDefault();
        e.stopPropagation();
    }

    formulario.classList.add('was-validated');
}