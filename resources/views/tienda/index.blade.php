@extends('layouts.main')

@section('titulo', 'Tiendas')

@section('cabecera')
    <x-cabecera subTitulo="Listado de Tiendas">
        <i class="fa-solid fa-cart-shopping"></i> Listado de Tiendas
    </x-cabecera>
@endsection

@section('contenido')

@if (session('mensaje'))
    <div class="card gradient-for-card-se mb-2">
        <div class="card-body text-center" style="background-color: #0e1321">
            <p class="card-text lead fs-4"> {{ session('mensaje') }} </p>
        </div>
    </div>
@endif

<x-listView  :registros="$tiendas" modelo="tienda" :campos="['id', 'nombre', 'ubicacion']" />

@endsection