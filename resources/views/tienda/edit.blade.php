@extends('layouts.main')

@section('titulo', 'Editar Tienda')

@section('contenido')
    <ul class="nav nav-tabs mb-5">
        <li class="nav-item">
            <a class="nav-link" aria-current="page" href="{{ route('tienda.show', $tienda->id) }}">Cliente {{ $tienda->id }}</a>
        </li>

        <li class="nav-item">
            <a class="nav-link active disabled" aria-disabled="true" href="{{ route('tienda.edit', $tienda->id) }}">Editar Cliente</a>
        </li>
        
        <li class="nav-item" style="margin-left: auto;">
            <a class="nav-link" aria-current="page" href="{{ route('tienda.show', $tienda->id) }}">Volver</a>
        </li>
    </ul>

    @if (session('mensaje'))
        <div class="card gradient-for-card-se mb-4">
            <div class="card-body text-center" style="background-color: #0e1321">
                <p class="card-text lead fs-4">{{ session('mensaje') }} </p>
            </div>
        </div>
    @endif

    <x-formularios.formulario :modelo="$tienda" :errors="$errors" method="PUT" route="{{ route('tienda.update', $tienda->id) }}">
        <x-formularios.input type="text" label="Nombre" name="nombre" :value="old('nombre', $tienda->nombre)"/>
        <x-formularios.input type="text" label="Ubicacion" name="ubicacion" :value="old('ubicacion', $tienda->ubicacion)"/>
    </x-formularios.formulario>
@endsection
