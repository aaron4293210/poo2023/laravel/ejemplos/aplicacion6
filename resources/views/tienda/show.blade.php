@extends('layouts.main')

@section('titulo', 'Mostrar')

@section('contenido')
<div class="row">
    <ul class="nav nav-tabs">
        <li class="nav-item">
            <a class="nav-link active disabled" aria-disabled="true">Tienda {{ $tienda->id }}</a>
        </li>
    
        <li class="nav-item">
            <a class="nav-link" href="{{ route('tienda.edit', $tienda->id) }}">Editar Cliente</a>
        </li>
        
        <li class="nav-item" style="margin-left: auto;">
            <a class="nav-link" aria-current="page" href="{{ route('tienda.index') }}">Volver</a>
        </li>
    </ul>

    @if (session('mensaje'))
        <div class="card gradient-for-card-se mt-4">
            <div class="card-body text-center" style="background-color: #0e1321">
                <p class="card-text lead fs-4"> {{ session('mensaje') }} </p>
            </div>
        </div>
    @endif
    
    <div class="col-lg-10 my-4 mb-sm-0 offset-lg-1">
        <div class="card text-center listadoTarjetaCliente">
            <div class="card-header">
                <span class="fs-3 text-center text-is-gradient">Tienda {{ $tienda->id }}</span>
            </div>

            <div class="card-body text-center">
                <i class="fa-solid fa-store fa-5x profile-pic text-gradient-primary mb-2"></i>
                <h4 class="card-title font-italic nombreListadoCliente my-4">{{ $tienda->nombre }}</h4>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="list-group">
                            <div class="list-group-item d-flex justify-content-between align-items-center p-3">
                                <span class="fw-bold text-uppercase">Ubicación:</span>
                                <span class="ms-auto">{{ $tienda->ubicacion }}</span>
                            </div>
                            
                        </div>
                    </div>
                </div>

                <div class="my-4 d-flex justify-content-center align-items-center">
                    <a class="btn btn-outline-success me-2" href="{{ route('tienda.edit', $tienda->id) }}" role="button">Editar</a>
                    
                    <form action="{{ route('tienda.destroy', $tienda)}}" method="POST">
                        @csrf @method('DELETE')
                        <button type="submit" class="btn btn-outline-danger">Borrar</button>
                    </form>      
                </div>
            </div>
        </div>
        <div class="gradient-for-card-tb"></div>
    </div>
</div>
@endsection