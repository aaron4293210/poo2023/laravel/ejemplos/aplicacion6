@extends('layouts.main')

@section('titulo', 'Crear Tienda')

@section('cabecera')
    <x-cabecera subTitulo="">
        <i class="fa-solid fa-cart-shopping"></i> Crear Tienda
    </x-cabecera>
@endsection

@section('contenido')
    <x-formularios.formulario :errors="$errors" route="{{ route('tienda.store') }}">
        <x-formularios.input type="text" label="Nombre" name="nombre" :value="old('nombre')"/>
        <x-formularios.input type="text" label="Ubicacion" name="ubicacion" :value="old('ubicacion')"/>
    </x-formularios.formulario>
@endsection
