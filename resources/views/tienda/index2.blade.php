@extends('layouts.main')

@section('titulo', 'Tiendas')

@section('contenido')

@if (session('mensaje'))
    <div class="card gradient-for-card-se mb-2">
        <div class="card-body text-center" style="background-color: #0e1321">
            <p class="card-text lead fs-4"> {{ session('mensaje') }} </p>
        </div>
    </div>
@endif

<div class="row">
    <h1 class="text-gradient-secondary text-center">Listado de Tiendas</h1>
    @foreach ($tiendas as $tienda)
    <div class="col-lg-12 mb-sm-0">
        <div class="card listadoTarjetaCliente" style="padding: 0.25rem">
            <div class="card-body d-flex align-items-center">
                <div class="col-lg-1 mb-sm-0">
                    <a href="{{ route('tienda.show', $tienda->id) }}"><i class="fa-solid fa-store fa-2x profile-pic text-gradient-primary"></i></a>
                </div>

                <div class="col-lg-8">
                    <h4 class="card-title font-italic nombreListadoCliente">{{ $tienda->nombre }}</h4>
                </div>

                <div class="col-lg-3 d-flex justify-content-end align-items-center">
                    <div>
                        <a class="btn btn-outline-primary" href="{{ route('tienda.show', $tienda->id) }}" role="button">Ver</a>
                        <a class="btn btn-outline-success" href="{{ route('tienda.edit', $tienda->id) }}" role="button">Editar</a>
                    </div>
                
                    <form action="{{ route('tienda.destroy', $tienda)}}" method="POST">
                        @csrf @method('DELETE')
                        <button type="submit" class="btn btn-outline-danger mx-1">Borrar</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @endforeach
</div>

<div class="row mt-3">
    {{-- Es el paginador --}}
    {{ $tiendas->links() }}
</div>
@endsection