@extends('layouts.main')

@section('titulo', 'Editar Producto')

@section('contenido')
    <ul class="nav nav-tabs mb-5">
        <li class="nav-item">
            <a class="nav-link" aria-current="page" href="{{ route('producto.show', $producto->id) }}">Producto {{ $producto->id }}</a>
        </li>

        <li class="nav-item">
            <a class="nav-link active disabled" aria-disabled="true" href="{{ route('producto.edit', $producto->id) }}">Editar Producto</a>
        </li>
        
        <li class="nav-item" style="margin-left: auto;">
            <a class="nav-link" aria-current="page" href="{{ route('producto.show', $producto->id) }}">Volver</a>
        </li>
    </ul>

    @if (session('mensaje'))
        <div class="card gradient-for-card-se mb-4">
            <div class="card-body text-center" style="background-color: #0e1321">
                <p class="card-text lead fs-4">{{ session('mensaje') }} </p>
            </div>
        </div>
    @endif
    
    <x-formularios.formulario :modelo="$producto" :errors="$errors" method="PUT" route="{{ route('producto.update', $producto->id) }}">
        <x-formularios.input type="text" label="Nombre" name="nombre" :value="old('nombre', $producto->nombre)"/>
        <x-formularios.input type="text" label="Precio" name="precio" :value="old('precio', $producto->precio)"/>
    </x-formularios.formulario>
@endsection
