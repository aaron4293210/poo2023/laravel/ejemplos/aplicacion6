@extends('layouts.main')

@section('titulo', 'Crear Producto')

@section('cabecera')
    <x-cabecera subTitulo="">
        <i class="fa-brands fa-r-project"></i> Crear Producto
    </x-cabecera>
@endsection

@section('contenido')
    <x-formularios.formulario :errors="$errors" route="{{ route('producto.store') }}">
        <x-formularios.input type="text" label="Nombre" name="nombre" :value="old('nombre')"/>
        <x-formularios.input type="number" label="Precio" name="precio" :value="old('precio')"/>
    </x-formularios.formulario>
@endsection
