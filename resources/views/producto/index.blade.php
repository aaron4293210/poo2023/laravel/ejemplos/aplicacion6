@extends('layouts.main')

@section('titulo', 'Tiendas')

@section('cabecera')
    <x-cabecera subTitulo="Listado de Productos">
        <i class="fa-brands fa-r-project"></i> Listado de Productos
    </x-cabecera>
@endsection

@section('contenido')

@if (session('mensaje'))
    <div class="card gradient-for-card-se mb-2">
        <div class="card-body text-center" style="background-color: #0e1321">
            <p class="card-text lead fs-4"> {{ session('mensaje') }} </p>
        </div>
    </div>
@endif

<x-listView  :registros="$productos" modelo="producto" :campos="['id', 'nombre', 'precio']" />

@endsection

@section('pie')
    <x-pie datos="Copyright 2024, Aaron Terry - Todos los derechos reservados" />
@endsection
