<footer class="transparent text-center text-lg-start p-3 pb-1 gradient-footer">
    <div class="container">
        <div class="row">
            <div class="col-lg-2 col-md-12 mb-4 mb-md-0">
            </div>

            <div class="col-lg-8 col-md-6 mb-4 mb-md-0">
                <ul class="nav justify-content-center">
                    <li class="nav-item">
                        <a class="nav-link {{ Request::routeIs('home.index') ? ' active' : '' }}" href="{{ route('home.index') }}">Inicio</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link {{ Request::routeIs(['tienda.index', 'tienda.create', 'tienda.edit', 'tienda.show']) ? ' active' : '' }}" href="{{ route('tienda.index') }}">Tiendas</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link {{ Request::routeIs(['producto.index', 'producto.create', 'producto.edit', 'producto.show']) ? ' active' : '' }}" href="{{ route('producto.index') }}">Productos</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link {{ Request::routeIs(['productoTienda.index', 'productoTienda.create', 'productoTienda.edit', 'productoTienda.show']) ? ' active' : '' }}" href="{{ route('productoTienda.index') }}">Productos Tiendas</a>
                    </li>
                </ul>
            </div>

            <div class="col-lg-2 col-md-6 mb-4 mb-md-0">
                <ul class="nav justify-content-center">
                    <li class="nav-item">
                        <a class="nav-link" href="#"><i class="fa-brands fa-twitter"></i></a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="#"><i class="fa-brands fa-instagram"></i></a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="#"><i class="fa-brands fa-linkedin"></i></a>
                    </li>
                </ul>
            </div>
        </div>

        <hr>
        
        <div class="row">
            <div class="col-lg-12 text-center">
                <p>
                    <i class="fa fa-copyright" aria-hidden="true"></i>
                    Copyright 2024, Aaron Terry - Todos los derechos reservados
                </p>  
            </div>
        </div>
    </div>
</footer>
