@extends('layouts.main')

@section('titulo', 'Mostrar')

@section('contenido')
<div class="row">
    <ul class="nav nav-tabs">
        <li class="nav-item">
            <a class="nav-link active disabled" aria-disabled="true">Producto Tienda {{ $productoTienda->id }}</a>
        </li>
    
        <li class="nav-item">
            <a class="nav-link" href="{{ route('productoTienda.edit', $productoTienda->id) }}">Editar Producto Tienda</a>
        </li>
        
        <li class="nav-item" style="margin-left: auto;">
            <a class="nav-link" aria-current="page" href="{{ route('productoTienda.index') }}">Volver</a>
        </li>
    </ul>

    @if (session('mensaje'))
        <div class="card gradient-for-card-se mt-4">
            <div class="card-body text-center" style="background-color: #0e1321">
                <p class="card-text lead fs-4"> {{ session('mensaje') }} </p>
            </div>
        </div>
    @endif
    
    <div class="col-lg-10 my-4 mb-sm-0 offset-lg-1">
        <div class="card text-center listadoTarjetaCliente">
            <div class="card-header">
                <span class="fs-3 text-center text-is-gradient">Producto Tienda {{ $productoTienda->id }}</span>
            </div>

            <div class="card-body text-center">
                <i class="fa-solid fa-shop-lock fa-5x profile-pic text-gradient-primary"></i>

                <div class="row mt-4">
                    <div class="col-lg-6">
                        <div class="list-group">
                            <div class="list-group-item d-flex justify-content-between align-items-center p-3">
                                <span class="fw-bold text-uppercase">ID Producto:</span>
                                <span class="ms-auto">{{ $productoTienda->producto_id }}</span>
                            </div>

                            <div class="list-group-item d-flex justify-content-between align-items-center p-3">
                                <span class="fw-bold text-uppercase">Cantidad:</span>
                                <span class="ms-auto">{{ $productoTienda->cantidad }}</span>
                            </div>

                            <div class="list-group-item d-flex justify-content-between align-items-center p-3">
                                <span class="fw-bold text-uppercase">Nombre Tienda:</span>
                                <span class="text-truncate ms-auto" style="max-width: 100%">{{ $productoTienda->tienda->nombre }}</span>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-lg-6">
                        <div class="list-group">
                            <div class="list-group-item d-flex justify-content-between align-items-center p-3">
                                <span class="fw-bold text-uppercase">ID Tienda:</span>
                                <span class="text-truncate ms-auto" style="max-width: 100%">{{ $productoTienda->tienda_id }}</span>
                            </div>

                            <div class="list-group-item d-flex justify-content-between align-items-center p-3">
                                <span class="fw-bold text-uppercase">Nombre Producto:</span>
                                <span class="text-truncate ms-auto" style="max-width: 100%">{{ $productoTienda->producto->nombre }}</span>
                            </div>
                            
                        </div>
                    </div>   
                </div>

                <div class="my-4 d-flex justify-content-center align-items-center">
                    <a class="btn btn-outline-success me-2" href="{{ route('productoTienda.edit', $productoTienda->id) }}" role="button">Editar</a>
                    
                    <form action="{{ route('productoTienda.destroy', $productoTienda)}}" method="POST">
                        @csrf @method('DELETE')
                        <button type="submit" class="btn btn-outline-danger">Borrar</button>
                    </form>      
                </div>
            </div>
        </div>
        <div class="gradient-for-card-tb"></div>
    </div>
</div>
@endsection