@extends('layouts.main')

@section('titulo', 'Crear Clientes')

@section('cabecera')
    @parent
@endsection

@section('contenido')
    <x-formularios.formulario :errors="$errors" route="{{ route('productoTienda.store') }}">
        <x-formularios.input type="number" label="Cantidad" name="cantidad" :value="old('nombre')"/>
        <x-formularios.input type="number" label="Producto ID" name="producto" :value="old('producto')"/>
        <x-formularios.input type="number" label="Tienda ID" name="tienda" :value="old('tienda')"/>
    </x-formularios.formulario>
@endsection
