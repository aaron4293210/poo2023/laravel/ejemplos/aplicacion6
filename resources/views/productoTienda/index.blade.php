@extends('layouts.main')

@section('titulo', 'Tiendas')

@section('cabecera')
    @parent
    <x-cabecera subTitulo="Listado de Productos Tiendas">
        <i class="fa-solid fa-boxes-stacked"></i> Listado de Productos Tiendas
    </x-cabecera>
@endsection

@section('contenido')
    @if (session('mensaje'))
        <div class="card gradient-for-card-se mb-2">
            <div class="card-body text-center" style="background-color: #0e1321">
                <p class="card-text lead fs-4"> {{ session('mensaje') }} </p>
            </div>
        </div>
    @endif

    <x-listviewrelations
        :registros="$productoTiendas"
        modelo="productoTienda"
        :campos="[
            'id',
            'cantidad',
            'producto_id',
            'tienda_id',
            'nombretienda',
            'nombreproducto',
            'tienda.nombre',
            'tienda.ubicacion',
            'producto.nombre',
            'producto.precio'
        ]"
    />
@endsection
