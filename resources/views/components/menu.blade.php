<nav class="navbar navbar-expand-lg navbar-dark transparent gradient-header2">
    <div class="container-fluid">
        <a class="navbar-brand" href="{{ route('home.index') }}">
            <h1 class="display-5 tituloMenu">{{ $tituloMenu }}</h1>
        </a>

        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse justify-content-end" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link {{ Request::routeIs('home.index') ? ' active' : '' }}" href="{{ route('home.index') }}">Inicio</a>
                </li>

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle {{ Request::routeIs(['tienda.index', 'tienda.create', 'tienda.edit', 'tienda.show']) ? ' active' : '' }}" href="#" role="button" data-bs-toggle="dropdown">
                        Tiendas
                    </a>

                    <ul class="dropdown-menu">
                        <li><a class="dropdown-item {{ Request::routeIs('tienda.index') ? ' active' : '' }}" href="{{ route('tienda.index') }}">Ver Tiendas</a></li>
                        <li><a class="dropdown-item {{ Request::routeIs('tienda.create') ? ' active' : '' }}" href="{{ route('tienda.create') }}">Insertar Tienda</a></li>
                    </ul>
                </li>

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle {{ Request::routeIs(['producto.index', 'producto.create', 'producto.edit', 'producto.show']) ? ' active' : '' }} " href="#" role="button" data-bs-toggle="dropdown">
                        Productos
                    </a>

                    <ul class="dropdown-menu">
                        <li><a class="dropdown-item {{ Request::routeIs('producto.index') ? ' active' : '' }}" href="{{ route('producto.index') }}">Ver Productos</a></li>
                        <li><a class="dropdown-item {{ Request::routeIs('producto.create') ? ' active' : '' }}" href="{{ route('producto.create') }}">Insertar Producto</a></li>
                    </ul>
                </li>

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle {{ Request::routeIs(['productoTienda.index', 'productoTienda.create', 'productoTienda.edit', 'productoTienda.show']) ? ' active' : '' }}" href="#" role="button" data-bs-toggle="dropdown">
                        Productos Tiendas
                    </a>

                    <ul class="dropdown-menu">
                        <li><a class="dropdown-item {{ Request::routeIs('productoTienda.index') ? ' active' : '' }}" href="{{ route('productoTienda.index') }}">Ver Productos Tiendas</a></li>
                        <li><a class="dropdown-item" href="{{ route('productoTienda.create') }}">Insertar Producto Tienda</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>
</div>
