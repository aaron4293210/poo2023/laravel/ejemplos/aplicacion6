# Objetivos

Componente anonimo que me permite mostrar un titulo como header en una web

# Uso

Para utilizarlo simplemente llamo al componente desde la vista y le coloco como contenido lo que quiero mostrar en la cabecera.

Me permite pasarle un parametro que seria el subtitulo

# Ejemplo
~~~php
    <x-cabecera subtitulo="Listado de Productos">
        <i class="fa-solid fa-cart-shopping"></i> Listado de Productos
    </x-cabecera>
~~~
# Parametros

- Slot
- Subtitulo (opcional)