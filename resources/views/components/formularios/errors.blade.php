@props(['errors'])

@if ($errors->any())
    <div class="card gradient-for-card-se mb-4">
        <div class="card-body text-center" style="background-color: #0e1321">
            <h2 class="card-title font-italic">Error</h2>
            @foreach ($errors->all() as $error)
                <p class="card-text lead fs-4"> {{ $error }} </p>
            @endforeach
        </div>
    </div>
@endif