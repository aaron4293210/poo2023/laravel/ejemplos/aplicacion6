@props(['name'])

@error($name)
    <p style="color: #dc3545;"> {{ $message }} </p>
@enderror