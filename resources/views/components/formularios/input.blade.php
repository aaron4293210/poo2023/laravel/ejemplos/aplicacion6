@props(['label', 'name', 'type' => 'text', 'value' => ''])

<div class="input-group mb-3">
    <div class="input-group has-validation">
        <div class="form-floating">
            <input type="{{ $type }}" class="form-control" id="{{ $name }}" name="{{ $name }}" value="{{ $value }}" required>
            <label class="form-label" for="{{ $name }}">{{ $label }}</label>
            <div id="{{ $name }}" class="invalid-feedback">
                Este campo es obligatorio.
            </div>
            
            <x-formularios.error name="{{ $name }}" />
        </div>
    </div>
</div>
