<x-formularios.errors :errors="$errors" />

<div class="row justify-content-center">
    <div class="col-lg-8 card">
        <div class="card-body">
            <form action="{{ $route }}" method="POST" class="card-body cardbody-color p-lg-5 needs-validation" novalidate>
                @csrf
                @method($method)

                {{ $slot }}

                <div class="text-center">
                    <button type="submit" class="btn btn-secondary btn-lg">{{ $boton }}</button>
                </div>
            </form>
        </div>
    </div>
</div>