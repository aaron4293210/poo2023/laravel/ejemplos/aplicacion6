@props([
    'modelo', 'registros', 'campos' => []
])

<div class="row">
    @foreach ($registros as $registro)
        <div class="col-lg-12 mb-sm-0">
            <div class="card listadoTarjetaCliente" style="padding: 0.25rem">
                <div class="card-body d-flex align-items-center">
                    <div class="col-lg-1 mb-sm-0">
                        <a href="{{ route('tienda.show', $registro->id) }}"><i class="fa-solid fa-store fa-2x profile-pic text-gradient-primary"></i></a>
                    </div>

                    <div class="col-lg-8">
                        <h4 class="card-title font-italic nombreListadoCliente">{{ $registro->nombre }}</h4>
                            {{-- Si campos no esta vacio solo muestro los campos pasados --}}
                            @if (!empty($campos))
                                @foreach ($campos as $value)
                                    <p class="card-text lead fs-4"> {{ $registro->getAttributeLabel($value) }}: {{ $registro->$value }} </p>
                                @endforeach
                            @else
                                @foreach ($registro->toArray() as $label => $value)
                                    <p class="card-text lead fs-4"> {{ $registro->getAttributeLabel($label) }}: {{ $value }} </p>
                                @endforeach
                            @endif
                    </div>

                    <div class="col-lg-3 d-flex justify-content-end align-items-center">
                        <div>
                            <a class="btn btn-outline-primary" href="{{ route($modelo.'.show', $registro->id) }}" role="button">Ver</a>
                            <a class="btn btn-outline-success" href="{{ route($modelo.'.edit', $registro->id) }}" role="button">Editar</a>
                        </div>
                    
                        <form action="{{ route($modelo.'.destroy', $modelo)}}" method="POST">
                            @csrf @method('DELETE')
                            <button type="submit" class="btn btn-outline-danger mx-1">Borrar</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
</div>

<div class="row mt-3">
    {{-- Comrpobamos si el metodo links existe --}}
    @if (method_exists($registros, 'links'))
        {{-- Es el paginador --}}
        {{ $registros->links() }}
    @endif
</div>