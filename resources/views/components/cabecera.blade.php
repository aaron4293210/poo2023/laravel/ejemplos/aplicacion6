@props(
    ['subTitulo' => '']
)

<section class="pt-5 text-center container">
    <div class="row py-lg-3">
        <div class="col-lg-6 col-md-8 mx-auto text-gradient-secondary">
            <h1 class="fw-light">{{ $slot }}</h1>
            <p class="lead text-muted ">{{ $subTitulo }}</p>
        </div>
    </div>
</section>
