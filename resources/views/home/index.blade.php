@extends('layouts.ladingPage')

@section('titulo', 'Inicio')

@section('contenido')
    <div class="gradient-header">
        <div class="imagen-landing">
            <nav class="navbar navbar-expand-lg navbar-dark transparent">
                <div class="container-fluid">
                    <a class="navbar-brand" href="{{ route('home.index') }}"><h1 class="display-5 tituloMenu">Aplicacion 6</h1></a>

                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse justify-content-end" id="navbarNav">
                        <ul class="navbar-nav">
                            <li class="nav-item">
                                <a class="nav-link {{ Request::routeIs('home.index') ? ' active' : '' }}" href="{{ route('home.index') }}">Inicio</a>
                            </li>
            
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle {{ Request::routeIs(['tienda.index', 'tienda.create']) ? ' active' : '' }}" href="#" role="button" data-bs-toggle="dropdown">
                                    Tiendas
                                </a>
            
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item {{ Request::routeIs('tienda.index') ? ' active' : '' }}" href="{{ route('tienda.index') }}">Ver Tiendas</a></li>
                                    <li><a class="dropdown-item {{ Request::routeIs('tienda.create') ? ' active' : '' }}" href="{{ route('tienda.create') }}">Insertar Tienda</a></li>
                                </ul>
                            </li>
            
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle {{ Request::routeIs(['producto.index', 'producto.create']) ? ' active' : '' }}" href="#" role="button" data-bs-toggle="dropdown">
                                    Productos
                                </a>
            
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item {{ Request::routeIs('producto.index') ? ' active' : '' }}" href="{{ route('producto.index') }}">Ver Productos</a></li>
                                    <li><a class="dropdown-item {{ Request::routeIs('producto.create') ? ' active' : '' }}" href="{{ route('producto.create') }}">Insertar Producto</a></li>
                                </ul>
                            </li>
            
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle {{ Request::routeIs(['productoTienda.index', 'productoTienda.create']) ? ' active' : '' }}" href="#" role="button" data-bs-toggle="dropdown">
                                    Productos Tiendas
                                </a>
            
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item {{ Request::routeIs('productoTienda.index') ? ' active' : '' }}" href="{{ route('productoTienda.index') }}">Ver Productos Tienda</a></li>
                                    <li><a class="dropdown-item" href="{{ route('productoTienda.create') }}">Insertar Producto Tienda</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>

            <div class="container my-5">
                <div class="row my-5">
                    <div class="col-lg-12 text-center">
                        <h1 class="title-landing text-gradient-secondary">Pagina de Inicio para todos <br> nuestros Clientes</h1> 
                        
                        <figure class="blockquote">
                            <blockquote class="blockquote">
                                <p>Tiendas con Productos</p>
                            </blockquote>
                            <figcaption class="blockquote-footer">
                                Someone famous in <cite title="Source Title">Source Title</cite>
                            </figcaption>
                        </figure>

                        <button class="btn btn-primary btn-lg">Productos</button>
                        <div class="circulo-morado"></div>
                    </div>
                </div>

                <h4 class="text-center">Confía en Nuestras Tiendas</h4>

                <div class="container  text-center">
                    <div class="animacion-landing-page p-0">
                        <div class="tiendas-container">
                            @foreach(range(1, 5) as $i)
                                <div class="mx-auto fs-4 fst-italic fw-bold p-4 w-auto"><i class="fa-solid fa-store"></i> Tienda {{ $i }}</div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container my-5">
        <div class="row row-cols-1 row-cols-md-3 g-4">
            <div class="col d-flex">
                <div class="card flex-fill">
                    <div class="card-body">
                        <div class="d-flex justify-content-center mt-2 mb-4">
                            <i class="fa-solid fa-users fa-4x text-gradient-primary"></i>
                        </div>
    
                        <h5 class="card-title">Quienes Somos</h5>
                        <p class="card-text">This is a longer card with supporting text below as a natural lead-in to
                            additional content. This content is a little bit longer.</p>
                        <figcaption class="blockquote-footer">
                            Someone famous in <cite title="Source Title">Source Title</cite>
                        </figcaption>
                    </div>
                    <div class="gradient-for-card-tb"></div>
                </div>
            </div>
    
            <div class="col d-flex">
                <div class="card flex-fill">
                    <div class="card-body">
                        <div class="d-flex justify-content-center mt-2 mb-4">
                            <i class="fa-solid fa-laptop-file fa-4x text-gradient-primary"></i>
                        </div>
    
                        <h5 class="card-title">Servicios</h5>
                        <p class="card-text">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                        <figcaption class="blockquote-footer">
                            Someone famous in <cite title="Source Title">Source Title</cite>
                        </figcaption>
                    </div>
                    <div class="gradient-for-card-tb"></div>
                </div>
            </div>
    
            <div class="col d-flex">
                <div class="card flex-fill">
                    <div class="d-flex justify-content-center mt-2 mb-4">
                        <i class="fa-brands fa-keybase fa-4x text-gradient-primary"></i>
                    </div>
    
                    <div class="card-body">
                        <h5 class="card-title">Iniciar Sesión</h5>
                        <p class="card-text">This is a longer card with supporting text below as a natural lead-in to
                            additional content.</p>
                        <figcaption class="blockquote-footer">
                            Someone famous in <cite title="Source Title">Source Title</cite>
                        </figcaption>
                    </div>
                    <div class="gradient-for-card-tb"></div>
                </div>
            </div>
        </div>
    </div>
@endsection
