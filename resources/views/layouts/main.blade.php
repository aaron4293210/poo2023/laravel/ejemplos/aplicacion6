<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('titulo')</title>
    @vite('resources/scss/app.scss')
</head>
<body>
    @include('comun._menu')
    @yield('cabecera')

    <div class="container my-4">
        @yield('contenido')
    </div>
    
    <x-pie datos="Copyright 2024, Aaron Terry - Todos los derechos reservados" />
    
    @vite('resources/js/app.js')
</body>
</html>