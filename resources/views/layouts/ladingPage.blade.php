<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    {{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.1/css/all.min.css" integrity="sha512-DTOQO9RWCH3ppGqcWaEA1BIZOC6xxalwEsw9c2QQeAIftl+Vegovlnee1c9QX4TctnWMn13TZye+giMm8e2LwA==" crossorigin="anonymous" referrerpolicy="no-referrer" /> --}}
    <title>@yield('titulo')</title>
    @vite('resources/scss/app.scss')
</head>
<body>
    <div>
        @yield('contenido')
    </div>
    
    <x-pie datos="Copyright 2024, Aaron Terry - Todos los derechos reservados" />
    
    @vite('resources/js/app.js')
</body>
</html>