<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\TiendaController;
use App\Http\Controllers\ProductoController;
use App\Http\Controllers\ProductotiendaController;
use Illuminate\Support\Facades\Route;

Route::controller(HomeController::class)->group(function () {
    Route::get('/', 'index')->name('index');
    Route::get('/index', 'index')->name('home.index');
});

Route::resource('tienda', TiendaController::class);
Route::resource('producto', ProductoController::class);
Route::resource('productoTienda', ProductotiendaController::class);