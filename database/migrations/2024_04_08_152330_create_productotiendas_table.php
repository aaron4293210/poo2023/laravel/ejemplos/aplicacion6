<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void {
        Schema::create('producto_tiendas', function (Blueprint $table) {
            $table->id();
            $table->integer('cantidad');
            // Crear FK de producto producto
            $table->unsignedBigInteger('producto_id');
            $table
                ->foreign('producto_id') // Nombre del campo que es FK
                ->references('id') // Nombre del campo en la tabla Principal
                ->on('productos') // Nombre de la tabla
                ->onDelete('cascade') // Propiedades FK
                ->onUpdate('cascade');
            // Crear FK de producto tienda
            $table->unsignedBigInteger('tienda_id');
            $table
                ->foreign('tienda_id') // Nombre del campo que es FK
                ->references('id') // Nombre del campo en la tabla Principal
                ->on('tiendas') // Nombre de la tabla
                ->onDelete('cascade') // Propiedades FK
                ->onUpdate('cascade');
            // Indexado sin duplicados
            $table->unique(['producto_id', 'tienda_id']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void {
        Schema::dropIfExists('producto_tiendas');
    }
};
