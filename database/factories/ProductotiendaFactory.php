<?php

namespace Database\Factories;

use App\Models\Productotienda;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Productotienda>
 */
class ProductotiendaFactory extends Factory {
    protected $model = Productotienda::class;
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array {
        return [
            'cantidad' => fake()->numberBetween(1, 1000),
        ];
    }
}
