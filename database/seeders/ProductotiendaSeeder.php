<?php

namespace Database\Seeders;

use App\Models\Producto;
use App\Models\Productotienda;
use App\Models\Tienda;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ProductotiendaSeeder extends Seeder{
    /**
     * Run the database seeds.
     */
    public function run(): void {
        
        for ($i = 0; $i < 50; $i++) {
            $tienda = Tienda::factory()->create();
            $producto = Producto::factory()->create();

            Productotienda::factory()
                ->for($tienda)
                ->for($producto)
                ->create();
        }
    }
}
