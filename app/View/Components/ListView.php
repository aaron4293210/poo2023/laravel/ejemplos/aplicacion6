<?php

namespace App\View\Components;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class ListView extends Component {
    public $registros;
    public string $modelo;
    public array $campos;
    public array $ajenas;

    /**
     * Create a new component instance.
     */
    public function __construct($registros, string $modelo, array $campos = [], array $ajenas = []) {
        $this->registros = $registros;
        $this->modelo = $modelo;
        $this->campos = $campos;
        $this->ajenas = $ajenas;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string {
        return view('components.listView');
    }
}
