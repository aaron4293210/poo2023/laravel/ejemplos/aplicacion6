<?php

namespace App\View\Components\formularios;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Formulario extends Component {
    public $producto;
    public $errors;
    public $boton;
    public $method;
    public $route;

    /**
     * Create a new component instance.
     */
    public function __construct($errors, $route, $producto = "", $boton = 'Guardar', $method = 'POST') {
        $this->producto = $producto;
        $this->errors = $errors;
        $this->boton = $boton;
        $this->method = $method;
        $this->route = $route;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string {
        return view('components.formularios.formulario');
    }
}
