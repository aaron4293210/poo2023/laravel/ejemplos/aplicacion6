<?php

namespace App\View\Components;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Listviewrelations extends Component {
    public $registros;
    public string $modelo;
    public array $campos;

    /**
     * Create a new component instance.
    */
    public function __construct($registros, string $modelo, array $campos = []) {
        $this->registros = $registros;
        $this->modelo = $modelo;

        // Leo los campos de mi tabla desde un regisro
        if (empty($campos)) {
            $campos = array_keys($registros[0]->getAttributes());
        }

        // Creamos un array para pasarselo a la vista para que solo lo recorra
        $this->campos = [];

        // Recorremos el argumentos campos
        foreach ($campos as $nomCampo) {
            // Compruebo si "$valor" contiene un punto
            if (str_contains($nomCampo, '.')) {
                // Hasta el punto esta el nombre de la relacion
                $relacion = substr($nomCampo, 0, strpos($nomCampo, '.'));
                // Desde el punto hasta el final es el nombre del campo
                $campo = substr($nomCampo, strpos($nomCampo, '.') + 1);

                $this->campos[] = ['valor' => function ($modelo) use ($relacion, $campo) {
                    return $modelo->$relacion->$campo;
                }, 'label' => $registros[0]->$relacion->getAttributeLabel($campo)];
            }
            else {
                $this->campos[] = ['label' => $registros[0]->getAttributeLabel($nomCampo), 'valor' => function ($modelo) use ($nomCampo) {
                    return $modelo->$nomCampo;
                }];
            }
        }
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string {
        return view('components.listviewrelations');
    }
}
