<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TiendaRequest extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
    */
    public function rules(): array {
        return [
            'nombre' => 'required|max:100|min:3',
            'ubicacion' => 'required',
        ];
    }

    /**
     *  Personalizo los nombres en los campos en los mensajes de error
    */
    public function messages(): array {
        return [
            'required' => 'El campo :attribute es obligatorio',
            'nombre.max' => 'El :attribute es demasiado largo',
            'min' => 'El :attribute es demasiado corto',
        ];
    }
    /**
     *  Personalizo los nombres en los campos en los mensajes de error
    */
    public function attributes(): array {
        return [
            'nombre' => 'nombre de la tienda',
            'ubicacion' => 'ubicación de la tienda',
        ];
    }
}
