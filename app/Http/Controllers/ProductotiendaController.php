<?php

namespace App\Http\Controllers;

use App\Models\Productotienda;
use Illuminate\Http\Request;

class ProductotiendaController extends Controller {
    /**
     * Display a listing of the resource.
     */
    public function index() {
        $productoTiendas = Productotienda::paginate(15);

        //dd($productoTiendas[0]->nombreProducto);

        return view('productoTienda.index', compact('productoTiendas'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create() {
        return view('productoTienda.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request) {
        $request->validate([
            'cantidad' => 'required|numeric|max:1000',
            'id_producto' => 'required',
            'id_tienda' => 'required',
        ]);

        $productoTienda = Productotienda::create($request->all());
        
        return redirect()
            ->route('productoTienda.show', $productoTienda->id)
            ->with('mensaje', 'Producto de la tienda creado con éxito')
        ;
    }

    /**
     * Display the specified resource.
     */
    public function show(int $id) {
        $productoTienda = Productotienda::find($id);

        

        return view('productoTienda.show', compact('productoTienda'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(int $id) {
        $productoTienda = Productotienda::find($id);

        return view('productoTienda.edit', compact('productoTienda'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Productotienda $productoTienda) {
        $request->validate([
            'cantidad' => 'required|numeric|max:1000',
            'id_producto' => 'required',
            'id_tienda' => 'required',
        ]);

        $productoTienda->update($request->all());

        return redirect()
            ->route('productoTienda.show', $productoTienda->id)
            ->with('mensaje', 'Producto de la tienda actualizado con éxito');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Productotienda $productoTienda) {
        $productoTienda->delete();
        
        return redirect()
            ->route('productoTienda.index')
            ->with('mensaje', 'Producto de la tienda borrado con éxito')
        ;
    }
}
