<?php

namespace App\Http\Controllers;

use App\Models\Producto;
use Illuminate\Http\Request;

class ProductoController extends Controller {
    /**
     * Display a listing of the resource.
     */
    public function index() {
        //$productos = Producto::paginate(15);
        $productos = Producto::all();

        //dd($productos[0]->getAttributeLabel('nombre'));

        return view('producto.index', compact('productos'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create() {
        return view('producto.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request) {
        $request->validate([
            'nombre' => 'required|string',
            'precio' => 'required|numeric|max:1000',
        ], [
            'required' => 'El campo :attribute es obligatorio.',
            'numeric' => 'El campo :attribute debe ser un valor numérico.',
            'max' => 'El campo :attribute no debe ser mayor a :max.',
        ], [
            'nombre' => 'Nombre Producto',
            'precio' => 'Precio Producto',
        ]);

        $prducto = Producto::create($request->all());
        
        return redirect()
            ->route('producto.show', $prducto->id)
            ->with('mensaje', 'Pruducto creado con éxito')
        ;
    }

    /**
     * Display the specified resource.
     */
    public function show(int $id) {
        $producto = Producto::find($id);

        return view('producto.show', compact('producto'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(int $id) {
        $producto = Producto::find($id);

        return view('producto.edit', compact('producto'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Producto $producto) {
        $request->validate([
            'nombre' => 'required|string',
            'precio' => 'required|numeric|max:1000',
        ], [
            'required' => 'El campo :attribute es obligatorio.',
            'numeric' => 'El campo :attribute debe ser un valor numérico.',
            'max' => 'El campo :attribute no debe ser mayor a :max.',
        ], [
            'nombre' => 'Nombre',
            'precio' => 'Precio',
        ]);

        $producto->update($request->all());

        return redirect()
            ->route('producto.show', $producto->id)
            ->with('mensaje', 'Prouducto actualizado con éxito')
        ;
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Producto $producto) {
        $producto->delete();
        
        return redirect()
            ->route('producto.index')
            ->with('mensaje', 'Producto borrado con éxito')
        ;
    }
}
