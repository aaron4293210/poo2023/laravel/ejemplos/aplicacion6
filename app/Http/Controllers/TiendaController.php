<?php

namespace App\Http\Controllers;

use App\Http\Requests\TiendaRequest;
use App\Models\Tienda;
use Illuminate\Http\Request;

class TiendaController extends Controller {
    /**
     * Display a listing of the resource.
     */
    public function index() {
        $tiendas = Tienda::paginate(15);

        return view('tienda.index', compact('tiendas'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create() {
        return view('tienda.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(TiendaRequest $request) {
        $tienda = Tienda::create($request->all());
        
        return redirect()
            ->route('tienda.show', $tienda->id)
            ->with('mensaje', 'Tienda creada con éxito')
        ;
    }

    /**
     * Display the specified resource.
     */
    public function show(int $id) {
        $tienda = Tienda::find($id);

        return view('tienda.show', compact('tienda'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(int $id) {
        $tienda = Tienda::find($id);

        return view('tienda.edit', compact('tienda'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(TiendaRequest $request, Tienda $tienda) {
        $tienda->update($request->all());

        return redirect()
            ->route('tienda.show', $tienda->id)
            ->with('mensaje', 'Tienda actualizada con éxito');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Tienda $tienda) {
        $tienda->delete();
        
        return redirect()
            ->route('tienda.index')
            ->with('mensaje', 'Tienda borrada con éxito')
        ;
    }
}
