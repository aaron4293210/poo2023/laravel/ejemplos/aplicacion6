<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\Schema;

class Producto extends Model {
    use HasFactory;

    protected $table = 'productos';
    protected $fillable = [
        'nombre',
        'precio',
    ];

    public function getFields() {
        return Schema::getColumnListing($this->table);
    }

    public static $labels = [
        'id' => 'ID Producto',
        'nombre' => 'Nombre Producto',
        'precio' => 'Precio Producto',
    ];

    public function productotiendas() : HasMany {
        return $this->hasMany(Productotienda::class);
    }

    public function getAttributeLabel($attribute) {
        return self::$labels[$attribute] ?? $attribute;
    }
}
