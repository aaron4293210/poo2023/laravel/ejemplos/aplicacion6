<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\Schema;

class Tienda extends Model {
    use HasFactory;

    protected $table = 'tiendas';
    protected $fillable = [
        'nombre',
        'ubicacion',
    ];

    public function getFields() {
        return Schema::getColumnListing($this->table);
    }

    public static $labels = [
        'id' => 'ID Tienda',
        'nombre' => 'Nombre Tienda',
        'ubicacion' => 'Ubicación Tienda',
    ];

    public function productotiendas() : HasMany {
        return $this->hasMany(Productotienda::class);
    }

    public function getAttributeLabel($attribute) {
        return self::$labels[$attribute] ?? $attribute;
    }
}
