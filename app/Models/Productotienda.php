<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\Schema;

class Productotienda extends Model {
    use HasFactory;

    protected $table = 'producto_tiendas';
    protected $fillable = [
        'cantidad',
        'producto_id',
        'tienda_id',
    ];

    public function getFields() {
        return Schema::getColumnListing($this->table);
    }

    public static $labels = [
        'id' => 'ID Producto Tienda',
        'producto_id' => 'Producto ID',
        'tienda_id' => 'Tienda ID',
        'nombretienda' => 'Nombre Tienda',
        'nombreproducto' => 'Nombre Producto',
    ];

    public function tienda() : BelongsTo {
        return $this->belongsTo(Tienda::class);
    }

    public function producto() : BelongsTo {
        return $this->belongsTo(Producto::class);
    }

    /**
     * Recupera el valor del atributo nombreTienda del modelo tienda.
     *
     * @return string El valor del atributo nombreTienda.
    */
    public function getNombreTiendaAttribute(): string {
        return $this->tienda->nombre;
    }

    public function getNombreProductoAttribute(): string {
        return $this->producto->nombre;
    }

    public function getAttributeLabel($attribute) {
        return self::$labels[$attribute] ?? $attribute;
    }
}
